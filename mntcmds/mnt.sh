#!/bin/bash

mkdir -p /mnt/USB

mount /dev/$1 /mnt/USB

cp --dereference /etc/resolv.conf /mnt/USB/etc/

mount --types proc /proc /mnt/USB/proc 

mount --rbind /sys /mnt/USB/sys 

mount --make-rslave /mnt/USB/sys 

mount --rbind /dev /mnt/USB/dev 

mount --make-rslave /mnt/USB/dev 

mount --bind /run /mnt/USB/run 

mount --make-slave /mnt/USB/run 

chroot /mnt/USB /bin/bash 