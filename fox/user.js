// custom settings for firefox 

user_pref("geo.enabled",false);
user_pref("media.peerconnection.enabled",false);
user_pref("privacy.resistFingerprinting",true);
user_pref("security.ssl3.rsa_des_ede3_sha",false);
user_pref("security.ssl.require_safe_negotiation",true);
user_pref("security.tls.enable_0rtt_data",false);
user_pref("plugin.scan.plid.all",false);
user_pref("privacy.firstparty.isolate",true);
user_pref("security.ssl.enable_false_start",false);
user_pref("webgl.disabled",true);
user_pref("browser.newtabpage.activity-stream.feeds.telemetry",false);
user_pref("browser.newtabpage.activity-stream.telemetry",false);
user_pref("browser.ping-centre.telemetry",false);
user_pref("toolkit.telemetry.updatePing.enabled",false);
user_pref("toolkit.telemetry.unified",false);
user_pref("toolkit.telemetry.shutdownPingSender.enabled",false);
user_pref("toolkit.telemetry.pioneer-new-studies-available",false);
user_pref("toolkit.telemetry.newProfilePing.enabled",false);
user_pref("toolkit.telemetry.firstShutdownPing.enabled",false);
user_pref("toolkit.telemetry.bhrPing.enabled",false);
user_pref("toolkit.telemetry.archive.enabled",false);
user_pref("browser.cache.insecure.enable",false);
user_pref("browser.cache.offline.enable",false);
user_pref("browser.cache.memory.enable",false);
user_pref("browser.cache.disk_cache_ssl",false);
user_pref("browser.cache.disk.enable",false);
user_pref("browser.formfil.enable",false);
//user_pref("javascript.enabled",false);
user_pref("javascript.enabled",true);
user_pref("browser.search.separatePrivateDefault",true);
user_pref("browser.search.separatePrivateDefault.ui.enabled",true);
user_pref("browser.search.suggest.enabled.private",true);
user_pref("browser.theme.dark-private-windows",true);

user_pref("app.update.notifyDuringDownload",true);
user_pref("browser.newtabpage.activity-stream.showSponsored",false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites",false);
user_pref("app.shield.optoutstudies.enabled",false);
user_pref("browser.contentblocking.category","strict");
user_pref("browser.laterrun.enabled",true);
user_pref("browser.search.region","US");
user_pref("browser.tabs.loadInBackground",true);
user_pref("network.http.referer.disallowCrossSiteRelaxingDefault",true);
user_pref("privacy.trackingprotection.enabled",true);
user_pref("privacy.trackingprotection.socialtracking.enabled",true);
user_pref("signon.autofillForms",false);
user_pref("signon.rememberSignons",false);
user_pref("datareporting.healthreport.uploadEnabled",false);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion",2);
user_pref("datareporting.policy.dataSubmissionEnabled",false);
user_pref("privacy.donottrackheader.enabled", true);

user_pref("services.sync.clients.lastSync", "0");
user_pref("services.sync.declinedEngines", "");
user_pref("services.sync.globalScore", 0);
user_pref("services.sync.nextSync", 0);
user_pref("services.sync.tabs.lastSync", "0");
user_pref("signon.autofillForms", false);
user_pref("signon.rememberSignons", false);

//Pocket
user_pref("extensions.pocket.enabled",false);
user_pref("services.sync.prefs.sync.browser.newtabpage.activity-stream.section.highlights.includePocket",false);
user_pref("extensions.pocket.onSaveRecs",false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket",false);
user_pref("extensions.pocket.showHome",false);

user_pref("dom.audioworklet.enabled",false);
user_pref("media.autoplay.block-webaudio",true);
user_pref("dom.webaudio.enabled",false);

user_pref("browser.download.useDownloadDir", false);
user_pref("browser.ctrlTab.sortByRecentlyUsed", true);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false);
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopSites", false);
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned", "");

user_pref("browser.newtabpage.activity-stream.feeds.snippets",	false);
user_pref("browser.newtabpage.activity-stream.enabled",		false);
user_pref("security.insecure_password.ui.enabled",		true);
user_pref("browser.chrome.site_icons",				false);
user_pref("browser.sessionstore.privacy_level",			2);
user_pref("signon.formlessCapture.enabled",			false);


//user_pref("browser.safebrowsing.malware.enabled",false);
//user_pref("browser.safebrowsing.phishing.enabled",false);
user_pref("app.normandy.enabled",false);
user_pref("browser.search.geoip.url","");
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned","");
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.searchEngines","");
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts","");
user_pref("extensions.pocket.showHome",false);
user_pref("media.autoplay.default",5);
user_pref("media.autoplay.block-event.enabled",true);
user_pref("extensions.ui.sitepermission.hidden",false);
user_pref("extensions.ui.locale.hidden",false);
user_pref("media.autoplay.default",5);
user_pref("media.autoplay.blocking_policy",2);
user_pref("browser.promo.focus.enabled",false);
user_pref("browser.vpn_promo.enabled",false);
user_pref("browser.download.always_ask_before_handling_new_types",true);
user_pref("browser.urlbar.suggest.quicksuggest.sponsored",false);
user_pref("browser.urlbar.sponsoredTopSites",false);
user_pref("browser.urlbar.quicksuggest.impressionCaps.sponsoredEnabled",false);
user_pref("browser.urlbar.placeholderName.private","DuckDuckGo");
user_pref("browser.urlbar.placeholderName","DuckDuckGo");
user_pref("browser.newtabpage.activity-stream.default.sites","");
user_pref("browser.newtabpage.activity-stream.discoverystream.enabled",false);
user_pref("browser.newtabpage.pinned","");
user_pref("browser.newtabpage.activity-stream.topSitesRows",0);
user_pref("extensions.formautofill.creditCards.enabled",false);
user_pref("extensions.formautofill.addresses.enabled",false);
user_pref("browser.warnOnQuit",false);
user_pref("browser.warnOnQuitShortcut",false);
user_pref("browser.quitShortcut.disabled",true);
/*
user_pref("");
*/
